function Table({ options, rowData }) {
  return (
    <div className="primary-table">
      <div className="primary-table__header">
        {options.columnDefs.map((def) => {
          return (
            <div key={def.field} className="primary-table__header__item">
              {def.headerName}
            </div>
          );
        })}
      </div>

      <div className="primary-table__body primary-scrollbar ">
        {rowData.map((row) => {
          return (
            <div key={row.id} className="primary-table__body__row">
              {options.columnDefs.map((def) => {
                return (
                  <div
                    key={def.field}
                    className="primary-table__body__row__item"
                  >
                    {def['cellRenderer']
                      ? def['cellRenderer'](row)
                      : row[def.field]}
                  </div>
                );
              })}
            </div>
          );
        })}
      </div>
    </div>
  );
}

export default Table;
