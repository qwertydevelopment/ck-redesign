import classNames from 'classnames';

function Card({ children, data }) {
  const { header, content, footer } = children;

  return (
    <div
      className={classNames(
        `h-full flex flex-col rounded-lg border border-black border-opacity-5`,
        {
          'px-4 md:px-8 pt-4 md:pt-8': footer,
          'p-4 md:p-8': !footer,
        },
      )}
    >
      {/* <!-- HEADER --> */}
      {header && (
        <div className="flex items-center justify-between">
          {/* Tittle */}
          <div className="font-avenir-black text-black text-sm md:text-base">
            {header.title}
          </div>
          {/* Header Action */}
          {header.action && (
            <div
              className="text-black text-opacity-60 text-sm cursor-pointer"
              onClick={header.actionHandler}
            >
              {header.action}
            </div>
          )}
        </div>
      )}

      {/* CONTENT */}
      {content && <div className="mt-3 md:mt-6">{content}</div>}

      {/* <!-- FOOTER --> */}
      {footer && (
        <div className="flex-grow -mx-4 md:-mx-8 bg-lightgray">{footer}</div>
      )}
    </div>
  );
}

export default Card;
