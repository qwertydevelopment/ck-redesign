import ChevronRight from '../../assets/images/chevron_right.svg';
import classNames from 'classnames';

export function Box({ children }) {
  const { type, icon, title, submit } = children;
  return (
    <div
      className={classNames(
        `h-full p-4 md:p-8 rounded-lg bg-${type} bg-opacity-10`,
      )}
    >
      {/* icon */}
      <div
        className={classNames(
          `w-12 h-12 bg-${type} rounded-full text-white flex items-center justify-center`,
        )}
      >
        <img src={icon} />
      </div>

      {/* title */}
      <div className="font-avenir-black text-lg md:text-xl mt-7 md:mt-14">
        {title}
      </div>

      <div
        className="flex items-center mt-2 md:mt-4 cursor-pointer"
        onClick={submit.handler}
      >
        <div className="font-avenir-book text-black text-opacity-50 mr-2">
          {submit.label}
        </div>
        <img src={ChevronRight} alt="submit" />
      </div>
    </div>
  );
}

export default Box;
