import classNames from 'classnames';

function Scrollable({ children, utilClasses }) {
  return (
    <div className={classNames('overflow-auto primary-scrollbar', utilClasses)}>
      {children}
    </div>
  );
}

export default Scrollable;
