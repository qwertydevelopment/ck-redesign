import classNames from 'classnames';

function Container({ children, utilClasses }) {
  return (
    <div className={classNames('container mx-auto h-full', utilClasses)}>
      {children}
    </div>
  );
}

export default Container;
