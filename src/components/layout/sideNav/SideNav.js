import { useState } from 'react';
import classNames from 'classnames';
// Images
import chevron_left from '../../../assets/images/chevron_left.svg';
import chevron_right from '../../../assets/images/chevron_right.svg';
import menu from '../../../assets/images/menu.svg';
import logo from '../../../assets/images/logo.svg';
import homeIcon from '../../../assets/images/home.svg';
import ordersIcon from '../../../assets/images/shopping_cart.svg';
import dollar from '../../../assets/images/dollar.svg';
import Question from '../../../assets/images/Question.svg';
import gear from '../../../assets/images/gear.svg';
import externalLink from '../../../assets/images/external_link.svg';
import { showHide } from '../../../lib/utils/class-names';

const primaryRoutes = [
  {
    icon: homeIcon,
    name: 'Home',
  },
  {
    icon: ordersIcon,
    name: 'Orders',
  },
  {
    icon: dollar,
    name: 'Payments',
  },
  {
    icon: Question,
    name: 'FAQ',
  },
  {
    icon: gear,
    name: 'Account Settings',
  },
];

const secondaryRoutes = [
  {
    name: 'Become a Merchant',
  },
  {
    name: 'Rewards',
  },
  {
    icon: externalLink,
    name: 'Help Center',
  },
];

function SideNav() {
  const [active, setActive] = useState({
    icon: homeIcon,
    name: 'Home',
  });

  const [extend, setExtend] = useState(true);

  const toggleExtend = () => {
    setExtend(!extend);
  };

  return (
    <div
      className={classNames(
        'h-full overflow-hidden bg-lightgray shadow pt-5 md:pt-10 transition-all',
        { 'w-56 md:w-72': extend, 'w-[56px] md:w-[88px]': !extend },
      )}
    >
      {/* Side nav Header   */}
      <div className="flex items-center pl-2 md:pl-4">
        <div
          className="flex items-center mr-2 md:mr-4 cursor-pointer shrink-0"
          onClick={toggleExtend}
        >
          <img
            src={chevron_left}
            alt="toggle"
            className={classNames(showHide(extend))}
          />
          <img src={menu} alt="toggle" />
          <img
            src={chevron_right}
            alt="toggle"
            className={classNames(showHide(!extend))}
          />
        </div>
        <img
          src={logo}
          alt="Credit Key"
          className={classNames('min-w-[133px]', showHide(extend))}
        />
      </div>
      {/* Side nav Body */}
      <div className="mt-6 md:mt-12">
        {/* <!-- Side nav Main navigaiton --> */}
        <div>
          {/* <!-- Side nav Main navigaiton Item --> */}
          {primaryRoutes.map((_r) => {
            return (
              <div
                onClick={() => {
                  setActive(_r);
                }}
                key={_r.name}
                className="relative cursor-pointer hover:bg-white transition flex items-center py-2 md:py-3"
              >
                {active.name === _r.name && (
                  <div className="absolute w-1 bg-primary h-full rounded-tr-md rounded-br-md"></div>
                )}
                <img
                  className="px-4 md:px-8 shrink-0 min-w-[18px]"
                  src={_r.icon}
                  alt={_r.name}
                />
                <div className="text-sm md:text-base font-avenir-book whitespace-nowrap">
                  {_r.name}
                </div>
              </div>
            );
          })}
        </div>

        <div className={classNames(showHide(extend))}>
          {/* <!-- Divider --> */}
          <div className="p-4 md:p-8">
            <div className=" border-t border-black border-opacity-5"></div>
          </div>

          {/* <!-- Side nav Secondary navigaiton --> */}
          <div>
            {/* <!-- Side nav Secondary navigaiton Item --> */}
            {secondaryRoutes.map((_r) => {
              return (
                <div
                  onClick={() => {
                    setActive(_r);
                  }}
                  key={_r.name}
                  className="flex items-center cursor-pointer hover:bg-white transition"
                >
                  <div className="font-avenir-book text-black text-opacity-60 text-xs md:text-sm pl-4 md:pl-8 py-2 md:py-3 whitespace-nowrap">
                    {_r.name}
                  </div>
                  {_r.icon && (
                    <img className="ml-1 md:ml-2" src={_r.icon} alt={_r.name} />
                  )}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SideNav;
