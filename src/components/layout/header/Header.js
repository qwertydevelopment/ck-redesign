import { useState } from 'react';
// Images
import slickAvatarIcon from '../../../assets/images/slick_avatar.svg';
import chevronDownIcon from '../../../assets/images/chevron_down.svg';

function Header() {
  return (
    <div className="flex items-center justify-between px-4 md:px-0 mb-6 md:mb-12">
      {/* <!-- Tittle --> */}
      <div className="font-avenir-black text-xl md:text-5xl">Dashboard</div>
      {/* <!-- User menu --> */}
      <div className="flex items-center">
        <div className="text-black text-opacity-60 text-sm">Slick Studio</div>
        <img className="mx-2" src={slickAvatarIcon} alt="avatar" />
        <img src={chevronDownIcon} alt="down" />
      </div>
    </div>
  );
}

export default Header;
