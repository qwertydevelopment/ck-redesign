const plugin = require("tailwindcss/plugin");

const scrollbar = {
    '.primary-scrollbar': {
        '&::-webkit-scrollbar': {
            width: '6px'
        },
        '&::-webkit-scrollbar-track': {
            background: '#f1f1f1',
        },
        '&::-webkit-scrollbar-thumb': {
            background: '#00000059',
            'border-radius': '5px',
            '&:hover': {
                background: '#555'
            }
        }
    }
}

const scrollbarPlugin = plugin(function ({ addComponents, theme }) {
    addComponents(scrollbar)
})

module.exports = scrollbarPlugin;