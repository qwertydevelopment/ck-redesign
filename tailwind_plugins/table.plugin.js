const plugin = require("tailwindcss/plugin");



const tablePlugin = plugin(function ({ addComponents, theme }) {

    addComponents(
        {
            '.primary-table': {
                padding: theme('padding.2'),
                paddingBottom: 0,
                paddingTop: 0,
                border: '1px solid rgba(0,0,0,0.05)',
                borderRadius: '8px',                
                '.primary-table__header': {
                    display: 'grid',
                    gridTemplateColumns: 'repeat(7, 1fr)',
                    alignItems: "center",
                    color: 'rgba(0, 0, 0, 0.6)',
                    fontFamily: theme('fontFamily.avenir-book'),
                    fontSize: theme('fontSize.sm'),
                    paddingLeft: theme('padding.2'),
                    paddingRight: theme('padding.2'),
                    paddingTop: theme('padding.2'),
                    paddingBottom: theme('padding.2'),
                    marginLeft: `-${theme('margin.2')}`,
                    marginRight: `-${theme('margin.2')}`,
                    borderBottom: '1px solid rgba(0,0,0,0.05)',
                    '.primary-table__header__item': {
                        whiteSpace: 'nowrap',
                        textOverflow: 'ellipsis',
                        overflow: 'hidden'
                    }
                },
                '.primary-table__body': {
                    marginLeft: `-${theme('margin.2')}`,
                    marginRight: `-${theme('margin.2')}`,
                    paddingLeft: theme('padding.2'),
                    paddingRight: theme('padding.2'),
                    overflow: 'auto',
                    height: theme('height.40'),
                    fontSize: theme('fontSize.xs'),
                    '.primary-table__body__row': {
                        display: 'grid',
                        gridTemplateColumns: 'repeat(7, 1fr)',
                        alignItems: "center",
                        paddingTop: theme('padding.2'),
                        paddingBottom: theme('padding.2'),
                        borderBottom: '1px solid rgba(0,0,0,0.05)',
                        '&:last-child': {
                            borderBottom: 'none',
                        },
                        '.primary-table__body__row__item': {
                            whiteSpace: 'nowrap',
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                            display: 'flex',
                            alignItems: 'center',
                            'img': {
                                display: 'none',
                                marginRight: theme('margin.2'),
                                width: theme('width.6')
                            }
                        }
                    }
                },

                '@media (min-width: 1024px)': {
                    padding: theme('padding.8'),
                    paddingBottom: 0,
                    paddingTop: 0,
                    '.primary-table__navigation': {
                        paddingBottom: theme('padding.8'),
                        fontSize: theme('fontSize.base'),
                        '.primary-table__navigation__item': {
                            marginRight: theme('margin.6')
                        }
                    },
                    '.primary-table__header': {
                        paddingLeft: theme('padding.8'),
                        paddingRight: theme('padding.8'),
                        paddingTop: theme('padding.4'),
                        paddingBottom: theme('padding.4'),
                        marginLeft: `-${theme('margin.8')}`,
                        marginRight: `-${theme('margin.8')}`,
                        color: 'rgba(0, 0, 0, 0.6)',
                        fontFamily: theme('fontFamily.avenir-book'),
                        fontSize: theme('fontSize.sm'),
                    },
                    '.primary-table__body': {
                        height: theme('height.60'),
                        fontSize: theme('fontSize.base'),
                        '.primary-table__body__row': {
                            paddingTop: theme('padding.4'),
                            paddingBottom: theme('padding.4'),
                            '.primary-table__body__row__item': {
                                'img': {
                                    display: 'block',
                                    marginRight: theme('margin.4'),
                                    width: 'auto'
                                }
                            }
                        }
                    },
                }
            }
        }
    )
})

module.exports = tablePlugin;