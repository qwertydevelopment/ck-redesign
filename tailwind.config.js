
const scrollbarPlugin = require("./tailwind_plugins/scrollbar.plugin");
const tablePlugin = require("./tailwind_plugins/table.plugin");

module.exports = {
  purge: false,
  mode: 'jit',
  content: [
    "./src/**/*.{js,jsx}",
  ],
  theme: {
    extend: {
      fontSize: {
        'xxs': '11px',
      },
      colors: {
        primary: "#0A60E0",
        secondary: "#680AE0",
        positive: "#1AAE5F",
        negative: '#E22D20',
        warning: '#FFC423',
        lightgray: '#F9F9F9',
        darkgray: '#E8EAEF'
      },
      fontFamily: {
        "avenir-black": ['Avenir-black'],
        "avenir-book": ['Avenir-Book'],
        "sf-medium": ["sf-compact-display-medium"],
        "sf-thin": ["sf-compact-display-thin"],
      },
      gridTemplateRows: {
        '7': 'repeat(7, minmax(0, 1fr))',
        '8': 'repeat(8, minmax(0, 1fr))',
        '9': 'repeat(9, minmax(0, 1fr))',
        '10': 'repeat(10, minmax(0, 1fr))'
      }
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
      xl: '1167px',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    scrollbarPlugin,
    tablePlugin
  ]
}

